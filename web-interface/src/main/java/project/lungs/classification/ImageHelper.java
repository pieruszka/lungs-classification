package project.lungs.classification;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.server.StreamResource;
import org.apache.commons.io.IOUtils;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

class ImageHelper {

    private ImageHelper() {
    }

    static Component saveImage(String mimeType, String fileName, InputStream stream) {
        if (mimeType.startsWith("image")) {
            Image image = new Image();
            try {
                byte[] bytes = IOUtils.toByteArray(stream);
                image.getElement().setAttribute("src", new StreamResource(
                        fileName, () -> new ByteArrayInputStream(bytes)));
                try (ImageInputStream in = ImageIO.createImageInputStream(new ByteArrayInputStream(bytes))) {
                    final Iterator<ImageReader> readers = ImageIO.getImageReaders(in);
                    if (readers.hasNext()) {
                        ImageReader current = readers.next();
                        try {
                            current.setInput(in);
                            ImageIO.write(current.read(0), "png", new File("webapp/tmp/uploaded.png"));
                        } finally {
                            current.dispose();
                        }
                    }
                }
                image.setWidth(400 + "px");
                image.setHeight(400 + "px");
            } catch (IOException e) {
                e.printStackTrace();
            }
            return image;
        }
        return new Div();
    }
}