package project.lungs.classification;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vaadin.flow.component.*;
import com.vaadin.flow.component.charts.Chart;
import com.vaadin.flow.component.charts.model.*;
import com.vaadin.flow.component.charts.model.style.SolidColor;
import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.html.*;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.menubar.MenuBarVariant;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.tabs.TabsVariant;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.MemoryBuffer;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.PWA;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;
import org.datavec.image.loader.NativeImageLoader;
import org.nd4j.linalg.api.ndarray.INDArray;
import project.lungs.classification.models.Alex;
import project.lungs.classification.models.Inception;

import javax.servlet.annotation.WebInitParam;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Route
@PWA(name = "Lungs Classification",
        shortName = "Lungs Classification App",
        description = "This is Lungs Classification application.",
        enableInstallPrompt = false)
@CssImport("./themes/webinterface/shared-styles.css")
@CssImport(
        themeFor = "vaadin-grid",
        value = "./themes/webinterface/shared-styles.css")
@CssImport(value = "./themes/webinterface/vaadin-text-field-styles.css")
@Theme(value = Lumo.class)
@PageTitle("System wspomagania klasyfikacji chorób płuc")
@WebInitParam(name = "compatibilityMode", value = "true")
public class MainView extends VerticalLayout {

    private static final String BASE_COLOR = "#2e6599";
    private static final String BANNER_IMG_LOCATION = "images/baner.png";
    private static final String BACKGROUND_URL = "url('images/background3.jpg')";
    private static final Alex ALEX = setupAlexModel();
    private static final Inception INCEPTION = setupInceptionModel();
    private final ObjectMapper mapper = new ObjectMapper();
    private ListDataProvider<Result> alexDataProvider = new ListDataProvider<>(Stream.of(Result.Case.values())
            .map(aCase -> new Result(aCase.getLabel(), null)).collect(Collectors.toList()));
    private ListDataProvider<Result> inceptionDataProvider = new ListDataProvider<>(Stream.of(Result.Case.values())
            .map(aCase -> new Result(aCase.getLabel(), null)).collect(Collectors.toList()));
    private Grid<Result> table = initTable();
    private Chart chart = initChart();
    private NativeImageLoader loader = new NativeImageLoader(240, 240, 3);
    private Div detailsPage = new Div();

    public MainView() {

        HorizontalLayout content = new HorizontalLayout();
        VerticalLayout loadingMenu = new VerticalLayout();
        VerticalLayout resultPart = new VerticalLayout();
        this.getElement().getStyle().set("background-image", BACKGROUND_URL);
        this.setSizeFull();

        MemoryBuffer buffer = new MemoryBuffer();
        Upload upload = new Upload(buffer);
        Div output = new Div();

        Tabs tabs = new Tabs();
        Div pages = new Div();
        setupTabs(tabs, pages);

        setupUpload(upload, output, buffer);
        loadingMenu.add(setupUploadLabel(), upload, output);
        loadingMenu.setAlignItems(Alignment.CENTER);
        loadingMenu.setWidth("800px");
        resultPart.add(createModelMenuBar(), tabs, pages);
        resultPart.setWidthFull();
        resultPart.setAlignItems(Alignment.CENTER);
        content.setVerticalComponentAlignment(Alignment.CENTER);
        content.setSpacing(true);
        content.add(loadingMenu, resultPart);
        content.setSizeFull();
        add(setupHeader(), content);
    }

    private static Alex setupAlexModel() {
        Alex alex = new Alex();
        alex.loadOrBuildModel(false);
        return alex;
    }

    private static Inception setupInceptionModel() {
        Inception inception = new Inception();
        inception.loadOrBuildModel(false);
        return inception;
    }

    private Label setupUploadLabel() {
        Label label = new Label(new String("Dodaj zdjęcie RTG płuc:".getBytes(), StandardCharsets.UTF_8));
        label.getStyle()
                .set("color", BASE_COLOR)
                .set("font-size", "30px")
                .set("font-weight", "bold")
                .set("font-family", "cursive");
        return label;
    }

    private HorizontalLayout setupHeader() {
        Image banner = new Image(BANNER_IMG_LOCATION, "Banner");
        banner.setHeight("300px");
        HorizontalLayout header = new HorizontalLayout();
        header.add(banner);
        return header;
    }

    private void setupUpload(Upload upload, Div output, MemoryBuffer buffer) {
        upload.addSucceededListener(event -> {
            String type = event.getMIMEType();
            if (!type.endsWith("jpg") && !type.endsWith("jpeg") && !type.endsWith("png")) {
                Notification.show("Błąd formatu zdjęcia");
            } else {
                Component component = ImageHelper.saveImage(type, event.getFileName(), buffer.getInputStream());
                Notification.show("Dodano zdjęcie");
                runCalculationAndShowResults(new File("webapp/tmp/uploaded.png"));
                output.removeAll();
                showOutput("Dodano zdjęcie" + " " + event.getFileName(), component, output);
            }
        });
        upload.addFileRejectedListener(event -> {
            output.removeAll();
            showOutput(event.getErrorMessage(), new Paragraph(), output);
        });
        upload.getElement().addEventListener("file-remove", event -> {
            output.removeAll();
        });
    }

    private void showOutput(String text, Component content, HasComponents output) {
        HtmlComponent p = new HtmlComponent(Tag.P);
        p.getElement().setText(text).getStyle().set("color", BASE_COLOR).set("font-weight", "bold");
        output.add(p);
        output.add(content);
    }

    private Grid<Result> initTable() {
        Grid<Result> grid = new Grid<>(Result.class);
        grid.setColumns("name", "percentage");
        grid.getColumnByKey("name").setHeader("Nazwa choroby");
        grid.getColumnByKey("percentage").setHeader("Prawdopodobieństwo");
        grid.getStyle().set("background-color", "transparent");
        grid.setAllRowsVisible(true);
        grid.setDataProvider(alexDataProvider);
        grid.setClassNameGenerator(item -> (item.getPercentage() == null || item.getPercentage() < 20) ? "table" : "tableRowHighlight");
        return grid;
    }

    private void setupTabs(Tabs tabs, Div pages) {
        Tab tab1 = new Tab(VaadinIcon.TABLE.create(), new Span("Wyniki"));
        Div page1 = new Div();
        page1.add(table);
        page1.setWidth("500px");
        page1.setVisible(true);

        Tab tab2 = new Tab(VaadinIcon.PIE_CHART.create(), new Span("Wykres"));
        Div page2 = new Div();
        page2.add(chart);
        page2.setVisible(false);

        Tab tab3 = new Tab(VaadinIcon.CALC.create(), new Span("Dokładność"));
        updateDetailsPageText("alex");
        detailsPage.setVisible(false);

        Map<Tab, Component> tabsToPages = new HashMap<>();
        tabsToPages.put(tab1, page1);
        tabsToPages.put(tab2, page2);
        tabsToPages.put(tab3, detailsPage);
        tabs.add(tab1, tab2, tab3);
        pages.add(page1, page2, detailsPage);

        tabs.addThemeVariants(TabsVariant.LUMO_CENTERED);
        tabs.addSelectedChangeListener(event -> {
            tabsToPages.values().forEach(page -> page.setVisible(false));
            Component selectedPage = tabsToPages.get(tabs.getSelectedTab());
            selectedPage.setVisible(true);
        });
        tabs.setWidth("700px");
    }

    private void updateDetailsPageText(String model) {
        ValidationDto result = null;
        try {
            result = mapper.readValue(new File("resources/modelStats/validation/" + model + "/details.json"), ValidationDto.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (result != null) {
            StringJoiner joiner = new StringJoiner("<br>&bull; ");
            joiner.add("<b>Dokładność modelu:</b>")
                    .add("Dokładność " + result.getAccuracy() * 100 + "%")
                    .add("Precyzja " + result.getPrecision() * 100 + "%")
                    .add("Czułość " + result.getRecall() * 100 + "%")
                    .add("Wartość F1 " + result.getF1Score() * 100 + "%");
            detailsPage.getElement()
                    .setProperty("color", BASE_COLOR)
                    .setProperty("font-size", "25px")
                    .setProperty("innerHTML", "<style> body {font-size: 20px;color: #2e6599;}</style>" + joiner.toString());
        }
    }

    private void runCalculationAndShowResults(File file) {
        INDArray image = null;
        try {
            image = loader.asMatrix(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        calculateAlexModel(image);
        calculateInceptionModel(image);
        refreshTableAndChart();
    }

    private void calculateAlexModel(INDArray image) {
        reloadDataProvider(ALEX.testModel(image), alexDataProvider);
    }

    private void calculateInceptionModel(INDArray image) {
        reloadDataProvider(INCEPTION.testModel(image), inceptionDataProvider);
    }

    private void refreshTableAndChart() {
        table.getDataProvider().refreshAll();
        DataProviderSeries<Result> series = new DataProviderSeries<>(inceptionDataProvider, Result::getPercentage);
        series.setX(Result::getName);
        chart.getConfiguration().setSeries(series);
        chart.drawChart();
    }

    private void reloadDataProvider(INDArray output, ListDataProvider<Result> provider) {
        Result.Case[] labels = Result.Case.values();
        double[] results = output.toDoubleVector();
        for (int i = 0; i < labels.length; i++) {
            String label = labels[i].getLabel();
            Double newValue = results[i] * 100;
            provider.getItems().stream()
                    .filter(result -> result.getName().equals(label))
                    .findFirst()
                    .ifPresent(result -> result.setPercentage(newValue));
        }
    }

    private MenuBar createModelMenuBar() {
        MenuBar menuBar = new MenuBar();
        ComponentEventListener<ClickEvent<MenuItem>> alexListener = e -> {
            table.setDataProvider(alexDataProvider);
            DataProviderSeries<Result> series = new DataProviderSeries<>(alexDataProvider, Result::getPercentage);
            series.setX(Result::getName);
            chart.getConfiguration().setSeries(series);
            alexDataProvider.refreshAll();
            updateDetailsPageText("alex");
            Notification.show("AlexNet");
        };
        ComponentEventListener<ClickEvent<MenuItem>> inceptionListener = e -> {
            table.setDataProvider(inceptionDataProvider);
            DataProviderSeries<Result> series = new DataProviderSeries<>(inceptionDataProvider, Result::getPercentage);
            series.setX(Result::getName);
            chart.getConfiguration().setSeries(series);
            inceptionDataProvider.refreshAll();
            updateDetailsPageText("inception");
            Notification.show("InceptionResNetV1");
        };

        menuBar.addItem("AlexNet", alexListener);
        menuBar.addItem("InceptionResNetV1", inceptionListener);
        menuBar.addThemeVariants(MenuBarVariant.LUMO_CONTRAST);
        chart.drawChart();
        return menuBar;
    }

    private Chart initChart() {
        chart = new Chart(ChartType.PIE);
        Configuration conf = chart.getConfiguration();
        Tooltip tooltip = new Tooltip();
        tooltip.setValueDecimals(1);
        conf.setTooltip(tooltip);
        PlotOptionsPie plotOptions = new PlotOptionsPie();
        plotOptions.setAllowPointSelect(true);
        plotOptions.setCursor(Cursor.POINTER);
        plotOptions.setShowInLegend(true);
        conf.setPlotOptions(plotOptions);
        conf.getChart().setBackgroundColor(new SolidColor(255, 255, 255, 0.0));
        chart.setWidth("600px");
        chart.setHeight("600px");
        chart.setVisibilityTogglingDisabled(true);
        return chart;
    }
}
