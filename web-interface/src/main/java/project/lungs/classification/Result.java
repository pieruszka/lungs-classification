package project.lungs.classification;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Result {

    private String name;
    private Double percentage;

    public Result(String name, Double percentage) {
        this.name = name;
        this.percentage = percentage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPercentage() {
        return percentage;
    }

    public void setPercentage(Double percentage) {
        this.percentage = BigDecimal.valueOf(percentage).setScale(3, RoundingMode.HALF_UP).doubleValue();
    }

    public enum Case {
        ATELECATSIS("Niedodma"),
        CARDIOMEGALY("Kardiomegalia"),
        CONSOLIDATION("Konsolidacja"),
        EDMA("Obrzęk"),
        EFFUSION("Wysięk"),
        EMPHYSEMA("Rozedma"),
        FIBROSIS("Zwłóknienie"),
        HERNIA("Przepuklina"),
        INFILTRATION("Naciek"),
        MASS("Guz"),
        NO_FINDING("Brak zmian"),
        NODULE("Guzek"),
        PLEURAL_THICKENING("Zgrubienie opłucnej"),
        PNEUMONIA("Zapalenie"),
        PNEUMOTHORAX("Odma");

        private String label;

        Case(String label) {
            this.label = label;
        }

        public String getLabel() {
            return label;
        }
    }
}
