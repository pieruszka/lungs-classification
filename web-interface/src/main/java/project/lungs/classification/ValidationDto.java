package project.lungs.classification;

class ValidationDto {
    private double precision;
    private double accuracy;
    private double recall;
    private double f1Score;

    public double getPrecision() {
        return precision;
    }

    public double getAccuracy() {
        return accuracy;
    }

    public double getRecall() {
        return recall;
    }

    public double getF1Score() {
        return f1Score;
    }
}
