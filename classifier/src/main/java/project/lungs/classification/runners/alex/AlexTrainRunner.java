package project.lungs.classification.runners.alex;

import project.lungs.classification.models.Alex;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

public class AlexTrainRunner {

    public static void main(String[] args) {

        Instant start = Instant.now();
        Alex alex = new Alex();
        alex.loadOrBuildModel(true);
        alex.setIterations(1);
        alex.trainModel();
        alex.evaluateModel();

        System.out.println("Runner ended. Total time: " + ChronoUnit.MINUTES.between(start, Instant.now()));
    }
}
