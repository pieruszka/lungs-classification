package project.lungs.classification.runners.alex;

import org.datavec.image.loader.NativeImageLoader;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler;
import project.lungs.classification.models.Alex;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;

public class AlexTestRunner {

    public static void main(String[] args) {

        Alex alex = new Alex();
        alex.loadOrBuildModel(false);
        NativeImageLoader loader = new NativeImageLoader(240, 240, 3);
        INDArray image = null;
        try {
            image = loader.asMatrix(new File("resources/posortowane/Atelectasis/00000011_006.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        INDArray indArray = alex.testModel(image);
        System.out.println("1: " + Arrays.toString(indArray.toDoubleVector()));
    }
}
