package project.lungs.classification.runners.alex;

import project.lungs.classification.models.Alex;

public class AlexValidationRunner {

    public static void main(String[] args) {

        Alex alex = new Alex();
        alex.setBatchSize(50);
        alex.loadOrBuildModel(false);
        alex.evaluateModel();
    }
}