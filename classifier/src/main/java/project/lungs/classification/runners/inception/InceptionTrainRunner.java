package project.lungs.classification.runners.inception;

import project.lungs.classification.models.Inception;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

public class InceptionTrainRunner {

    public static void main(String[] args) {

        Instant start = Instant.now();
        Inception inception = new Inception();
        inception.loadOrBuildModel(true);
        inception.setBatchSize(10);
        inception.setIterations(1);
        inception.trainModel();
        inception.evaluateModel();

        System.out.println("Runner ended. Total time: " + ChronoUnit.MINUTES.between(start, Instant.now()));
    }
}
