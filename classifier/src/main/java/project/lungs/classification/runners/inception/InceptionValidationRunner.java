package project.lungs.classification.runners.inception;

import project.lungs.classification.models.Inception;

public class InceptionValidationRunner {

    public static void main(String[] args) {

        Inception inception = new Inception();
        inception.setBatchSize(50);
        inception.loadOrBuildModel(false);
        inception.evaluateModel();
    }
}

