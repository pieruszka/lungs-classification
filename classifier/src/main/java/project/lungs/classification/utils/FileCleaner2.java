package project.lungs.classification.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

public class FileCleaner2 {

    public static void main(String[] args) {

        File photosDirectory = new File("C:/Users/Adam/Desktop/Magisterka/posortowane/Atelectasis — kopia");
        File labelsFile = new File("C:/Users/Adam/projects/samples/labels.csv");

        Map<String, String> records = new HashMap<>();
        int w = 0;
        int d = 0;
        try (BufferedReader br = new BufferedReader(new FileReader(labelsFile))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(";");
                String k = values[0];
                String v = values[1];
                if (v.contains("|") && v.contains("Atelectasis")) {
                    d++;
                }
                if (v.contains("Atelectasis")) {
                    w++;
                }
                records.put(k, v);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("W " + w + ", d " + d);
/*
        AtomicInteger allFoundCount = new AtomicInteger(0);
        AtomicInteger deletedCount = new AtomicInteger(0);
        List<String> notFound = new ArrayList<>();
        File[] photos = photosDirectory.listFiles();
        for (File file : photos) {
            String fileName = file.getName();
            if (records.containsKey(fileName)) {
                allFoundCount.incrementAndGet();
                String nameFromCsv = records.get(fileName);
                if (nameFromCsv.contains("|")) {
                    file.delete();
                    deletedCount.incrementAndGet();
                }
            } else {
                notFound.add(fileName);
            }
        }
        System.out.println("Wszystkich było " + allFoundCount.get());
        System.out.println("Nie znaleziono w csv " + notFound.size() + " -> " + notFound.toString());
        System.out.println("Usunięto " + deletedCount.get());

 */
    }
}
