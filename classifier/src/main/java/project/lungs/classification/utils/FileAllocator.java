package project.lungs.classification.utils;

import org.apache.commons.io.FileUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class FileAllocator {

    public static void main(String[] args) {

        File photosDirectory = new File("C:/Users/Adam/Desktop/Magisterka/pluca/images_012/images");
        File labelsFile = new File("C:/Users/Adam/projects/samples/labels.csv");

        Map<String, List<String>> records = new HashMap<>();
        try (BufferedReader br = new BufferedReader(new FileReader(labelsFile))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(";");
                String k = values[0];
                String v = values[1];
                String[] splittedValues = v.split("\\|");
                records.put(k, Arrays.asList(splittedValues));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        AtomicInteger successes = new AtomicInteger(0);
        List<String> fails = new ArrayList<>();
        File[] photos = photosDirectory.listFiles();
        Arrays.stream(photos).forEach(file -> {
                    String fileName = file.getName();
                    if (records.containsKey(fileName)) {
                        successes.incrementAndGet();
                        records.get(fileName).stream().forEach(s -> {
                            String newPath = "C:/Users/Adam/Desktop/Magisterka/posortowane/" + s + "/" + fileName;
                            try {
                                File newFile = new File(newPath);
                                if (!newFile.exists()) {
                                    FileUtils.copyFile(file, newFile);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        });
                    } else {
                        fails.add(fileName);
                    }
                }
        );
        System.out.println("Sukcesy " + successes.get());
        System.out.println("Problem z plikami " + fails.toString());
    }
}
