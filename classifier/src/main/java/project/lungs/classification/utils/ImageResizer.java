package project.lungs.classification.utils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

public class ImageResizer {
    static String pathname = "C:/Users/Adam/Desktop/Magisterka";

    public static void main(String[] args) {
        File[] subDirectories = new File(pathname + "/walidacyjne").listFiles();
        Arrays.stream(subDirectories).forEach(subDir ->
                Arrays.stream(subDir.listFiles()).forEach(file -> {
                    try {
                        loadAndResize(subDir.getName(), file.getName());
                    } catch (IOException e) {
                        System.out.println("Błąd dla " + subDir.getName() + "/" + file.getName());
                    }
                }));

    }

    static void loadAndResize(String subDir, String file) throws IOException {
        BufferedImage img = null;
        img = ImageIO.read(new File(pathname + "/walidacyjne/" + subDir + "/" + file));
        BufferedImage resizeImage = resizeImage(img, 240, 240);
        File output = new File(pathname + "/walidacyjne/" + subDir + "/" + file);
        ImageIO.write(resizeImage, "png", output);
    }


    static BufferedImage resizeImage(BufferedImage originalImage, int targetWidth, int targetHeight) throws IOException {
        BufferedImage resizedImage = new BufferedImage(targetWidth, targetHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics2D = resizedImage.createGraphics();
        graphics2D.drawImage(originalImage, 0, 0, targetWidth, targetHeight, null);
        graphics2D.dispose();
        return resizedImage;
    }
}
