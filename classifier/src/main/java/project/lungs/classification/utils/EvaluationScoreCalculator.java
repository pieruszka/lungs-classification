package project.lungs.classification.utils;

import org.json.JSONObject;

import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

public class EvaluationScoreCalculator {

    private final Map<Integer, Integer> tpMap = new HashMap<>();
    private final Map<Integer, Integer> tnMap = new HashMap<>();
    private final Map<Integer, Integer> fpMap = new HashMap<>();
    private final Map<Integer, Integer> fnMap = new HashMap<>();
    private final List<Map<Integer, Integer>> totalScores = List.of(tpMap, tnMap, fpMap, fnMap);
    private final String modelType;

    public EvaluationScoreCalculator(String modelType) {
        this.modelType = modelType;
    }

    public void calculateAndSaveStats() {
        JSONObject details = new JSONObject();
        double tp = calculateScore(tpMap);
        double tn = calculateScore(tnMap);
        double fp = calculateScore(fpMap);
        double fn = calculateScore(fnMap);

        double accuracy = (tp + tn) / (tp + fp + fn + tn);
        double precision = tp / (tp + fp);
        double recall = tp / (tp + fn);
        double f1Score = 2 * recall * precision / (recall + precision);
        details.put("precision", BigDecimal.valueOf(precision).setScale(3, RoundingMode.HALF_UP).toString());
        details.put("accuracy", BigDecimal.valueOf(accuracy).setScale(3, RoundingMode.HALF_UP).toString());
        details.put("recall", BigDecimal.valueOf(recall).setScale(3, RoundingMode.HALF_UP).toString());
        details.put("f1Score", BigDecimal.valueOf(f1Score).setScale(3, RoundingMode.HALF_UP).toString());
        writeToFile(details.toString(), "resources/modelStats/validation/" + modelType + "/details.json");
    }

    private double calculateScore(Map<Integer, Integer> values) {
        return values.values().stream().reduce(0, Integer::sum);
    }

    private void writeToFile(String value, String fileName) {
        try (FileWriter file = new FileWriter(fileName)) {
            file.write(value);
            file.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void recalculateScore(Map<Integer, Integer> tpScore, Map<Integer, Integer> tnScore,
                                 Map<Integer, Integer> fpScore, Map<Integer, Integer> fnScore) {
        List<Map<Integer, Integer>> newScores = List.of(tpScore, tnScore, fpScore, fnScore);
        IntStream.range(0, 4).forEach(i -> addToScore(newScores.get(i), totalScores.get(i)));
    }

    private void addToScore(Map<Integer, Integer> calculatedValues, Map<Integer, Integer> totalValues) {
        calculatedValues.forEach((key, value) -> {
            if (totalValues.containsKey(key)) {
                totalValues.put(key, totalValues.get(key) + value);
            } else {
                totalValues.put(key, value);
            }
        });
    }

}
