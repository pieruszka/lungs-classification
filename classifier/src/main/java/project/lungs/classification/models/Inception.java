package project.lungs.classification.models;

import org.deeplearning4j.nn.conf.CacheMode;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.ui.model.stats.StatsListener;
import org.deeplearning4j.util.ModelSerializer;
import org.deeplearning4j.zoo.model.InceptionResNetV1;
import org.nd4j.linalg.api.ndarray.INDArray;

import java.io.File;
import java.io.IOException;
import java.util.function.Consumer;
import java.util.function.Function;

public class Inception extends ModelBase<ComputationGraph> {

    public Inception() {
        super(
                "resources/inceptionModel1.zip",
                "resources/modelStats/ui-stats3.dl4j.inception2"
        );
    }

    public void trainModel() {
        trainAndSave(model::fit);
    }

    public void evaluateModel() {
        evaluate(model::outputSingle, "inception");
    }

    public INDArray testModel(INDArray image){
        return test(model::outputSingle, image);
    }

    @Override
    Function<String, ComputationGraph> restore() {
        return file -> {
            try {
                return ModelSerializer.restoreComputationGraph(file);
            } catch (IOException e) {
                log.error("Error on restoring Inception model", e);
            }
            return null;
        };
    }

    @Override
    ComputationGraph buildModel() {
        return InceptionResNetV1.builder()
                .inputShape(new int[]{channels, height, width})
                .numClasses(outputNum)
                .seed(rngseed)
                .build()
                .init();
    }

    @Override
    Consumer<StatsListener> setListeners() {
        return model::setListeners;
    }

    @Override
    Consumer<CacheMode> setCacheMode() {
        return model::setCacheMode;
    }
}