package project.lungs.classification.models;

import org.datavec.api.io.labels.ParentPathLabelGenerator;
import org.datavec.api.split.FileSplit;
import org.datavec.image.loader.NativeImageLoader;
import org.datavec.image.recordreader.ImageRecordReader;
import org.deeplearning4j.core.storage.StatsStorage;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.nn.api.Model;
import org.deeplearning4j.nn.conf.CacheMode;
import org.deeplearning4j.ui.api.UIServer;
import org.deeplearning4j.ui.model.stats.StatsListener;
import org.deeplearning4j.ui.model.storage.FileStatsStorage;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.evaluation.classification.Evaluation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import project.lungs.classification.utils.EvaluationScoreCalculator;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Function;

abstract class ModelBase<T extends Model> {

    protected final int height = 240;
    protected final int width = 240;
    protected final int channels = 3;
    protected final int rngseed = 123;
    protected final int outputNum = 15;
    private final Random randNumGen = new Random(rngseed);
    private final boolean saveUpdater = true;
    private final FileSplit trainData = new FileSplit(new File("resources/posortowane"), NativeImageLoader.ALLOWED_FORMATS, randNumGen);
    private final FileSplit validationData = new FileSplit(new File("resources/walidacyjne"), NativeImageLoader.ALLOWED_FORMATS, randNumGen);
    private final ImageRecordReader recordReader = new ImageRecordReader(height, width, channels, new ParentPathLabelGenerator());
    private final DataNormalization scaler = new ImagePreProcessingScaler(0, 1);
    private final String statsLocation;
    protected Logger log = LoggerFactory.getLogger(getClass());
    protected int batchSize = 40;
    protected int iterations = 1;
    protected boolean restoreModel = true;
    private String modelLocation;
    protected T model;

    ModelBase(String modelLocation, String statsLocation) {
        this.modelLocation = modelLocation;
        this.statsLocation = statsLocation;
    }

    public void setIterations(int iterations) {
        this.iterations = iterations;
    }

    public void setBatchSize(int batchSize) {
        this.batchSize = batchSize;
    }

    public void setRestoreModel(boolean restoreModel) {
        this.restoreModel = restoreModel;
    }

    public void loadOrBuildModel(boolean withModelListener) {
        if (restoreModel) {
            log.info("***** LOAD TRAINED MODEL ********");
            model = restore().apply(modelLocation);

        } else {
            log.info("**** BUILD MODEL ****");
            model = buildModel();
        }
        if (withModelListener) {
            setModelListeners(setListeners(), setCacheMode());
        }
    }

    void trainAndSave(Consumer<DataSetIterator> modelFitter) {
        log.info("***** TRAIN MODEL ********");
        for (int i = 0; i < iterations; i++) {
            DataSetIterator dataIter = prepareIterationData();
            Instant iterationStart = Instant.now();
            modelFitter.accept(dataIter);
            log.info(i + " iteration time: " + ChronoUnit.SECONDS.between(iterationStart, Instant.now()));
        }
        save(model);
    }

    void evaluate(Function<INDArray, INDArray> outputFunction, String statsType) {
        log.info("***** EVALUATE MODEL ********");
        initializeRecordReader(validationData);
        DataSetIterator iterator = new RecordReaderDataSetIterator(recordReader, batchSize, 1, outputNum);
        scaler.fit(iterator);
        iterator.setPreProcessor(scaler);
        Evaluation eval = new Evaluation(outputNum);
        EvaluationScoreCalculator scoreCalculator = new EvaluationScoreCalculator(statsType);

        while (iterator.hasNext()) {
            DataSet current = iterator.next();
            INDArray outputs = outputFunction.apply(current.getFeatures());
            eval.eval(current.getLabels(), outputs);
            scoreCalculator.recalculateScore(eval.truePositives(), eval.trueNegatives(), eval.falsePositives(),
                    eval.falseNegatives());
        }
        scoreCalculator.calculateAndSaveStats();
    }

    INDArray test(Function<INDArray, INDArray> function, INDArray image) {
        scaler.transform(image);
        return function.apply(image);
    }

    private void setModelListeners(Consumer<StatsListener> setListeners, Consumer<CacheMode> setCacheMode) {
        UIServer uiServer = UIServer.getInstance();
        StatsStorage statsStorage = new FileStatsStorage(new File(statsLocation));
        uiServer.attach(statsStorage);
        setListeners.accept(new StatsListener(statsStorage));
        setCacheMode.accept(CacheMode.DEVICE);
    }

    private DataSetIterator prepareIterationData() {
        initializeRecordReader(trainData);
        DataSetIterator dataIter = new RecordReaderDataSetIterator(recordReader, batchSize, 1, outputNum);
        scaler.fit(dataIter);
        dataIter.setPreProcessor(scaler);
        return dataIter;
    }

    private void initializeRecordReader(FileSplit data) {
        if (recordReader.resetSupported()) {
            recordReader.reset();
        }
        try {
            recordReader.initialize(data);
        } catch (IOException e) {
            log.error("Error on initializing RecordReader", e);
        }
    }

    private void save(T model) {
        log.info("***** SAVE TRAINED MODEL ********");
        try {
            ModelSerializer.writeModel(model, modelLocation, saveUpdater);
        } catch (IOException e) {
            log.error("Error on saving model", e);
        }
    }

    abstract Function<String, T> restore();

    abstract T buildModel();

    abstract Consumer<StatsListener> setListeners();

    abstract Consumer<CacheMode> setCacheMode();
}