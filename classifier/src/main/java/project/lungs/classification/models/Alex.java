package project.lungs.classification.models;

import org.deeplearning4j.nn.conf.CacheMode;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.ui.model.stats.StatsListener;
import org.deeplearning4j.util.ModelSerializer;
import org.deeplearning4j.zoo.model.AlexNet;
import org.nd4j.linalg.api.ndarray.INDArray;

import java.io.IOException;
import java.util.function.Consumer;
import java.util.function.Function;

public class Alex extends ModelBase<MultiLayerNetwork> {

    public Alex() {
        super(
                "resources/alexModel3.zip",
                "resources/modelStats/ui-stats3.dl4j.alex3"
        );
    }

    public void trainModel() {
        trainAndSave(model::fit);
    }

    public void evaluateModel() {
        evaluate(model::output, "alex");
    }

    public INDArray testModel(INDArray image) {
        return test(model::output, image);
    }

    @Override
    Function<String, MultiLayerNetwork> restore() {
        return file -> {
            try {
                return ModelSerializer.restoreMultiLayerNetwork(file);
            } catch (IOException e) {
                log.error("Error on restoring Alex model", e);
            }
            return null;
        };
    }

    @Override
    MultiLayerNetwork buildModel() {
        return AlexNet.builder()
                .inputShape(new int[]{channels, height, width})
                .numClasses(outputNum)
                .seed(rngseed)
                .build()
                .init();
    }

    @Override
    Consumer<StatsListener> setListeners() {
        return model::setListeners;
    }

    @Override
    Consumer<CacheMode> setCacheMode() {
        return model::setCacheMode;
    }
}